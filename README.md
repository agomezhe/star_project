# star_project

In this repository, you will find all the validation data used in the copil-star meeting on the 27th of January of 2021.


**detection-results:** You can find all the files(*.txt) with the detections obtained with the fusion-process algorithm.

	- First_val_old
	- First_val_new
	- Second_val_old
	- Second_val_new

**ground-truth:** You can find all the files(*.txt) with the objects labeled in the KITTI MoSeg dataset for the two validations.
	
	- First_val_old
	- First_val_new
	- Second_val_old
	- Second_val_new


**images-optional:** In this folder, it is possible to find all the images(*.png) used in the two validations.
	
	- First 
	- Second

**main_val.py:** Python script used to validate the fusion process considering the longitudinal distance of thirty meters.


**Related links:**

**MODNet:** https://webdocs.cs.ualberta.ca/~vis/kittimoseg/

**mAP:** https://github.com/Cartucho/mAP



